package nl.devoxx.projects.sounds.components.center.music;

import java.util.Arrays;

public enum Song {
    MAMMA_MIA("EDEDsDDEFEDsEDsGGGGFDs"),
    HAVANA("GBBGGEsBABCBAGBBGGE");

    public String musicNotes;

    Song(String musicNotes) {
        this.musicNotes = musicNotes;
    }

    public static boolean isSongCorrect(String musicNotes) {
        return Arrays.stream(Song.values()).anyMatch(song -> song.musicNotes.equals(musicNotes));
    }
}

