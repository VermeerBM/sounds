package nl.devoxx.projects.sounds.components.center.music;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class UserMusicSheet {
    private final static Canvas canvas = new Canvas(640, 150);
    private final static GraphicsContext gc = canvas.getGraphicsContext2D();
    final Image image = new Image("gsleutel.png");

    public void drawNote(String rowline) {

        // needs some attention, add mechanism draw (good green and bad red) lines
        if(rowline.contains("playNote(E")) {
            gc.setFill(Color.GREEN);
            gc.fillOval(80, 65, 20, 20);
        } else if(rowline.contains("playNote(F")){
            gc.setFill(Color.RED);
            gc.fillOval(120, 65, 20, 20);
        }
    }

    public Canvas createCanvas() {
        final double W = image.getWidth();
        final double H = image.getWidth();

        gc.drawImage(image, 0,0,50, 100);
        gc.setFill(Color.BLACK);
        gc.setStroke(Color.GREY);
        gc.setLineWidth(2);
        gc.strokeLine(0, 10, 700, 10);
        gc.strokeLine(0, 30, 700, 30);
        gc.strokeLine(0, 45, 700, 45);
        gc.strokeLine(0, 60, 700, 60);
        gc.strokeLine(0, 75, 700, 75);

        return canvas;
    }

}
