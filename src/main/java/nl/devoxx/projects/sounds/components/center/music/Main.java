package nl.devoxx.projects.sounds.components.center.music;


import javax.sound.midi.*;
import java.util.stream.IntStream;

import static nl.devoxx.projects.sounds.components.center.music.Note.*;

public class Main {

    private Synthesizer midiSynth;
    private Instrument[] instr;
    private MidiChannel[] mChannels;



    public void init() {
        try {
            midiSynth = MidiSystem.getSynthesizer();
            midiSynth.open();

            //get and load default instrument and channel lists
            instr = midiSynth.getDefaultSoundbank().getInstruments();
            mChannels = midiSynth.getChannels();

            midiSynth.loadInstrument(instr[0]);//load an instrument
        } catch (MidiUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void playNote(Note note, int channel, long time) {
        int notenumber = note.getMidiNumber();
        mChannels[channel].noteOn(notenumber, 1000);//On channel 0, play note number 60 with velocity 100
        sleep(time);
        mChannels[channel].noteOff(notenumber);//turn of the note
    }

    public void playNote(Note note) {
        playNote(note, 0, 1000);
    }

    public void playNote(Note note, long time) {
        playNote(note, 0, time);
    }

    public void playChord(Chord chord) {
        long time = 1000;
        playNote(chord.fst, 0, 1000);
        playNote(chord.snd, 1, 1000);
        playNote(chord.thrd, 2, 1000);

    }

    public void sleep(long time) {
        try {
            Thread.sleep(time); // wait time in milliseconds to control duration
        } catch (InterruptedException e) {
        }
    }

    public void playHavana() {
        playNote(G,250);
        playNote(B,250);
        playNote(B,250);

        playNote(G,250);

        playNote(G,250);
        playNote(E,250);

        sleep(450);

        playNote(B,250);
        playNote(A,250);
        playNote(B,250);

        playNote(C_,250);

        playNote(B,250);
        playNote(A,250);

        playNote(G,250);
        playNote(B,250);
        playNote(B,250);

        playNote(G,250);

        playNote(G,250);
        playNote(E,250);
    }

    public void playMamaMia() {
        playNote(E,250);
        playNote(D,250);
        playNote(E,250);
        playNote(D,250);

        sleep(200);

        playNote(D,250);
        playNote(D,250);
        playNote(E,250);
        playNote(FS,250);
        playNote(E,250);
        playNote(D,250);

        sleep(200);

        playNote(E,600);
        playNote(D,600);

        sleep(200);

        playNote(G,350);
        playNote(G,250);
        playNote(G,250);
        playNote(G,250);
        playNote(FS,450);
        playNote(D,250);

        sleep(1000);

    }

    public void playLoveYourself() {
        playNote(E,125);
        playNote(E,250);
        playNote(FS,250);
        playNote(GS,350);
        playNote(GS,250);
        playNote(FS,250);
        playNote(FS,250);
        playNote(FS,250);
        playNote(GS,250);
        playNote(E,250);
        playNote(E,250);

        sleep(500);

        playNote(FS,125);
        playNote(FS,250);
        playNote(GS,250);
        playNote(A,250);
        playNote(A,250);
        playNote(GS,250);
        playNote(FS,250);
        playNote(E,250);
        playNote(FS,250);
        playNote(GS,250);
        playNote(FS,250);

        sleep(200);

        playNote(E,250);
        playNote(E,250);
        playNote(FS,250);
        playNote(GS,250);
        playNote(B,250);
        playNote(FS,250);
        playNote(E,250);
        playNote(FS,250);
        playNote(GS,250);
        playNote(FS,250);
        playNote(E,250);
        playNote(E,250);

        sleep(200);


        playNote(FS,250);
        playNote(FS,250);
        playNote(GS,250);
        playNote(A,250);
        playNote(A,250);
        playNote(GS,250);
        playNote(FS,250);
        playNote(E,250);
        playNote(FS,250);
        playNote(GS,250);
        playNote(FS,250);

        sleep(1000);
    }




    public static void main(String[] args) {
        Main main = new Main();
        main.init();
        IntStream.rangeClosed(1,100)
                .forEach(i -> main.playMamaMia());
    }
}
