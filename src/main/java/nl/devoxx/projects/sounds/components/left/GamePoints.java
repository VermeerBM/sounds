package nl.devoxx.projects.sounds.components.left;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class GamePoints {

    public VBox createLeftBox() {
        VBox vBox = new VBox();
        vBox.setMaxWidth(400);
        vBox.setMaxHeight(200);

        Label totalPointsText = new Label();
        totalPointsText.setText("Leader board");
        totalPointsText.setFont(Font.font("Verdana", FontWeight.BOLD, 20));

        Label label = new Label();
        label.setText("Team Rocket: 200 POINTS");

        Label label2 = new Label();
        label2.setText("Pokemon Gym leader: 190 POINTS");

        Label label3 = new Label();
        label3.setText("Ash Ketchum: 180 POINTS");

        Label label4 = new Label();
        label4.setText("Misty: 100 POINTS");

        Label label5 = new Label();
        label5.setText("Brock: 50 POINTS");

        vBox.getChildren().addAll(totalPointsText, label, label2, label3, label4, label5);
        return vBox;
    }
}
