package nl.devoxx.projects.sounds.components.center.music;

import javax.sound.midi.*;


public class MiniMusicApp {   // this is the first one

    public static void main(String[] args) {
        MiniMusicApp mini = new MiniMusicApp();
        mini.play();
    }

    public void play() {

        try {

            // make (and open) a sequencer, make a sequence and track

            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            // now make two midi events (containing a midi message)
            MidiEvent event = null;

            // first make the message
            // then stick the message into a midi event
            // and add the event to the track
            ShortMessage first = new ShortMessage();
            first.setMessage(192,1,49,0);
            MidiEvent firstMessage = new MidiEvent(first,0);
            track.add(firstMessage);

            ShortMessage a = new ShortMessage();
            a.setMessage(144, 1, 44, 100);
            MidiEvent noteOn = new MidiEvent(a, 1); // <-- means at tick one, the above event happens
            track.add(noteOn);

            ShortMessage b = new ShortMessage();
            b.setMessage(128, 1, 44, 100);
            MidiEvent noteOff = new MidiEvent(b, 3); // <-- means at tick one, the above event happens
            track.add(noteOff);

            // add the events to the track

            // add the sequence to the sequencer, set timing, and start
            sequencer.setSequence(seq);

            sequencer.start();
            // new
            Thread.sleep(1000);
            sequencer.close();
            System.exit(0);
        } catch (Exception ex) {ex.printStackTrace();}
    } // close play

} // close class
