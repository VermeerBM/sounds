package nl.devoxx.projects.sounds.components.center.music

import javax.sound.midi.*

var sixteenth = 125L
var eigth = sixteenth * 2
var quarter = eigth * 2
var half = quarter * 2
var whole = half * 2

class SynthPlayer {

    val midiSynth = MidiSystem.getSynthesizer()
    var instr: Array<Instrument>
    var mChannels: Array<MidiChannel>
    var velocity = 1000


    init {
            midiSynth.open()
            //get and load default instrument and channel lists
            instr = midiSynth.defaultSoundbank.instruments
            mChannels = midiSynth.channels

            midiSynth.loadInstrument(instr!![0])//load an instrument
    }


    fun playNote(note: Note, time: Long = 1000) {
        val notenumber = note.midiNumber
        mChannels[0].noteOn(notenumber, velocity)//On channel 0, play note number 60 with velocity 100
        sleep(time)
        mChannels[0].noteOff(notenumber)//turn of the note
    }

    infix fun note(note: Note) = playNote(note, 250)
    infix fun shortNote(note: Note) = playNote(note, 125)
    infix fun longNote(note: Note) = playNote(note, 500)

    infix fun sixteenthNote(note: Note) = playNote(note, sixteenth)
    infix fun eighthNote(note: Note) = playNote(note, eigth)
    infix fun quaterhNote(note: Note) = playNote(note, quarter)
    infix fun halfNote(note: Note) = playNote(note, half)
    infix fun wholeNote(note: Note) = playNote(note, whole)

    infix fun sleep(time: Long) = Thread.sleep(time) // wait time in milliseconds to control duration

    infix fun part (action: () -> Unit) : Unit = action.invoke();

}
