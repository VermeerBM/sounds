package nl.devoxx.projects.sounds.components.center.music;

public class Commands {
    public static final String PLAY_NOTE_COMMAND = "playNote(";
    public static final String _C_COMMAND = PLAY_NOTE_COMMAND + "_C";
    public static final String _CS_COMMAND = PLAY_NOTE_COMMAND + "_CS";
    public static final String _D_COMMAND = PLAY_NOTE_COMMAND + "_D";
    public static final String _DS_COMMAND = PLAY_NOTE_COMMAND + "_DS";
    public static final String _E_COMMAND = PLAY_NOTE_COMMAND + "_E";
    public static final String _F_COMMAND = PLAY_NOTE_COMMAND + "_F";
    public static final String _FS_COMMAND = PLAY_NOTE_COMMAND + "_FS";
    public static final String _G_COMMAND = PLAY_NOTE_COMMAND + "_G";
    public static final String _GS_COMMAND = PLAY_NOTE_COMMAND + "_GS";
    public static final String _A_COMMAND = PLAY_NOTE_COMMAND + "_A";
    public static final String _AS_COMMAND = PLAY_NOTE_COMMAND + "_AS";
    public static final String _B_COMMAND = PLAY_NOTE_COMMAND + "_B";
    public static final String C_COMMAND = PLAY_NOTE_COMMAND + "C";
    public static final String CS_COMMAND = PLAY_NOTE_COMMAND + "CS";
    public static final String D_COMMAND = PLAY_NOTE_COMMAND + "D";
    public static final String DS_COMMAND = PLAY_NOTE_COMMAND + "DS";
    public static final String E_COMMAND = PLAY_NOTE_COMMAND + "E";
    public static final String F_COMMAND = PLAY_NOTE_COMMAND + "F";
    public static final String FS_COMMAND = PLAY_NOTE_COMMAND + "FS";
    public static final String G_COMMAND = PLAY_NOTE_COMMAND + "G";
    public static final String GS_COMMAND = PLAY_NOTE_COMMAND + "GS";
    public static final String A_COMMAND = PLAY_NOTE_COMMAND + "A";
    public static final String AS_COMMAND = PLAY_NOTE_COMMAND + "AS";
    public static final String B_COMMAND = PLAY_NOTE_COMMAND + "B";
    public static final String SLEEP_COMMAND = "sleep(";

    public static String playAndReturnNote(String rowLine) {
        Main main = new Main();
        main.init();

        if(rowLine.contains(SLEEP_COMMAND)) {
            main.sleep(Long.valueOf(rowLine.substring(6,9)));
        } else if (rowLine.contains(_CS_COMMAND)) {
            main.playNote(Note._CS,Long.valueOf(rowLine.substring(13,14)));
        } else if (rowLine.contains(_C_COMMAND)) {
            main.playNote(Note._C,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(_DS_COMMAND)) {
            main.playNote(Note._DS,Long.valueOf(rowLine.substring(13,14)));
        } else if (rowLine.contains(_D_COMMAND)) {
            main.playNote(Note._D,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(_E_COMMAND)) {
            main.playNote(Note._E    ,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(_FS_COMMAND)) {
            main.playNote(Note._FS,Long.valueOf(rowLine.substring(13,14)));
        } else if (rowLine.contains(_F_COMMAND)) {
            main.playNote(Note._F,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(_GS_COMMAND)) {
            main.playNote(Note._GS,Long.valueOf(rowLine.substring(13,14)));
        } else if (rowLine.contains(_G_COMMAND)) {
            main.playNote(Note._G,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(_AS_COMMAND)) {
            main.playNote(Note._AS,Long.valueOf(rowLine.substring(13,14)));
        } else if (rowLine.contains(_A_COMMAND)) {
            main.playNote(Note._A,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(_B_COMMAND)) {
            main.playNote(Note._B,Long.valueOf(rowLine.substring(12,14)));
        } else if (rowLine.contains(CS_COMMAND)) {
            main.playNote(Note.CS,Long.valueOf(rowLine.substring(12,15)));
        } else if (rowLine.contains(C_COMMAND)) {
            main.playNote(Note.C,Long.valueOf(rowLine.substring(12,15)));
        } else if (rowLine.contains(DS_COMMAND)) {
            main.playNote(Note.DS,Long.valueOf(rowLine.substring(12,15)));
        } else if (rowLine.contains(D_COMMAND)) {
            main.playNote(Note.D,Long.valueOf(rowLine.substring(11,14)));
        } else if (rowLine.contains(E_COMMAND)) {
            main.playNote(Note.E, Long.valueOf(rowLine.substring(11, 14)));
        } else if (rowLine.contains(FS_COMMAND)) {
            main.playNote(Note.FS,Long.valueOf(rowLine.substring(12,15)));
        } else if (rowLine.contains(F_COMMAND)) {
            main.playNote(Note.F,Long.valueOf(rowLine.substring(11,14)));
        } else if (rowLine.contains(GS_COMMAND)) {
            main.playNote(Note.GS,Long.valueOf(rowLine.substring(12,15)));
        } else if (rowLine.contains(G_COMMAND)) {
            main.playNote(Note.G,Long.valueOf(rowLine.substring(11,14)));
        } else if (rowLine.contains(AS_COMMAND)) {
            main.playNote(Note.AS,Long.valueOf(rowLine.substring(12,15)));
        } else if (rowLine.contains(A_COMMAND)) {
            main.playNote(Note.A,Long.valueOf(rowLine.substring(11,14)));
        } else if (rowLine.contains(B_COMMAND)) {
            main.playNote(Note.B,Long.valueOf(rowLine.substring(11,14)));
        }

        if(rowLine.startsWith("sleep")) {
            return rowLine.substring(0, 1);
        } else {
            return rowLine.substring(9, 10);
        }

    }
}
