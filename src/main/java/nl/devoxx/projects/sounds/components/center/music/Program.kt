package nl.devoxx.projects.sounds.components.center.music

import nl.devoxx.projects.sounds.components.center.music.Note.*

val play = SynthPlayer()

fun main(args: Array<String>) {

    var deel1 = {
        herhaal(2) {
            play longNote G
            play longNote A
            play longNote B
            play longNote G
        }
    }

    var deel2 = {
        herhaal(2) {
            //Bro - ther john
            play longNote B
            play longNote C_
            play longNote D_
            play sleep 500
        }
    }







//    herhaal(100) {
//        speel rust half
//        speel achtsteNoot A
//        speel achtsteNoot B
//        speel kwartNoot B
//
//        speel achtsteNoot B
//        speel achtsteNoot B
//        speel kwartNoot B
//        speel kwartNoot B
//        speel kwartNoot G
//        speel kwartNoot G
//
//        speel rust kwart
//
//        speel achtsteNoot  A
//        speel achtsteNoot  B
//        speel kwartNoot B
//
//        speel achtsteNoot B
//        speel achtsteNoot A
//        speel kwartNoot B
//        speel achtsteNoot B
//        speel achtsteNoot A
//        speel achtsteNoot G
//        speel achtsteNoot G
//        speel achtsteNoot E
//
//        speel rust kwart
//
//        speel achtsteNoot E
//        speel achtsteNoot B
//        speel kwartNoot B
//
//        speel halveNoot C_
//        speel achtsteNoot B
//        speel achtsteNoot G
//        speel kwartNoot G
//
//        speel halveNoot C_
//        speel achtsteNoot B
//        speel kwartNoot G
//
//        speel achtsteNoot G
//        speel halveNoot A
//        speel achtsteNoot B
//        speel kwartNoot A
//        speel halveNoot E
//
//        speel rust kwart
//        }


    }


//translate into Dutch DSL
val speel = play
infix fun SynthPlayer.rust(milli:Long) = sleep(milli)
infix fun SynthPlayer.zestiendeNoot(note: Note) = sixteenthNote(note)
infix fun SynthPlayer.achtsteNoot(note: Note) = eighthNote(note)
infix fun SynthPlayer.kwartNoot(note: Note) = quaterhNote(note)
infix fun SynthPlayer.halveNoot(note: Note) = halfNote(note)
infix fun SynthPlayer.holeNoot(note: Note) = wholeNote(note)

var zestiende = sixteenth
var achtste = eigth
var kwart = quarter
//half =  half
var heel = whole

fun herhaal(aantal:Int, actie: (Int) -> Unit):Unit = repeat(aantal, actie)

infix fun SynthPlayer.stuk (actie: () -> Unit) : Unit = part(actie);


fun playBrotherJohn() {
    play sleep 200

    //Are you slee - ping
    play longNote G
    play longNote A
    play longNote B
    play longNote G

    //Are you slee - ping
    play longNote G
    play longNote A
    play longNote B
    play longNote G

    //Bro - ther john
    play longNote B
    play longNote C_
    play longNote D_
    play sleep 500

    //Bro - ther john
    play longNote B
    play longNote C_
    play longNote D_
    play sleep 500

    //Morning bells are ring - ing
    play note D_
    play note E_
    play note D_
    play note C_
    play longNote B
    play longNote G

    //Morning bells are ring - ing
    play note D_
    play note E_
    play note D_
    play note C_
    play longNote B
    play longNote G

    //Ding dang dong
    play longNote G
    play longNote D
    play longNote G
    play sleep 500

    //Ding dang dong
    play longNote G
    play longNote D
    play longNote G
    play sleep 500
}





