package nl.devoxx.projects.sounds.components.header;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Logo {

    public ImageView createHeader() {
        Image logo = new Image("logo.png", 440, 150, false, true);
        ImageView imageViewLogo = new ImageView(logo);

        return imageViewLogo;
    }
}
