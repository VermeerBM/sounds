package nl.devoxx.projects.sounds.components.center.music;

public enum Chord {

    G_CHORD(Note.G, Note.B, Note.D),
    D_CHORD(Note.D, Note.FS, Note.A),
    EM_CHORD(Note.E, Note.G, Note.B),
    C_CHORD(Note.C, Note.E, Note.G);


    public Note fst;
    public Note snd;
    public Note thrd;

    Chord(Note fst, Note snd, Note thrd) {
        this.fst = fst;
        this.snd = snd;
        this.thrd = thrd;
    }
}
