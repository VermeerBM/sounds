package nl.devoxx.projects.sounds.components.center.userinput;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import nl.devoxx.projects.sounds.components.center.music.UserMusicSheet;
import nl.devoxx.projects.sounds.components.center.music.Song;
import nl.devoxx.projects.sounds.components.center.music.Commands;

public class CodeFields {
    private static final String STRING_PREFILLED_TEXT = "playNote(E,250)";
    private static final String STRING_STYLE = "-fx-highlight-fill: lightgray; -fx-highlight-text-fill: firebrick; -fx-font-size: 20px;";
    private static final String STRING_BUTTON = "Speel af";
    public static final String STRING_CORRECT_SONG = "Correct";
    public static final String STRING_INCORRECT_SONG = "Het nummer is nog niet goed";
    private static Label songCorrectLabel = new Label("");


    public VBox createUserInput() {
        VBox codeBlock = new VBox();
        ObservableList<Node> observableList = codeBlock.getChildren();

        UserMusicSheet userMusicSheet = new UserMusicSheet();
        observableList.add(userMusicSheet.createCanvas());

        // TextArea
        final TextArea textArea = new TextArea();
        textArea.setText(STRING_PREFILLED_TEXT);
        textArea.setStyle(STRING_STYLE);
        textArea.setMaxWidth(640);
        observableList.add(textArea);

        // Button to Append text
        Button appendButton = new Button(STRING_BUTTON);
        StringBuilder toBePlayedMusicLine = new StringBuilder();
        // Action event.
        appendButton.setOnAction(event -> {

            final String[] textAreaText = textArea.getText().split("\n");
            for(String rowline : textAreaText) {
                userMusicSheet.drawNote(rowline);
                if(!rowline.equals("")) {
                    toBePlayedMusicLine.append(Commands.playAndReturnNote(rowline.trim()));
                }
            }

            if (Song.isSongCorrect(toBePlayedMusicLine.toString())) {
                songCorrectLabel.setText(STRING_CORRECT_SONG);
                textArea.deleteText(0, textArea.getLength());
                // render new assignment
                // play sound correct
                // add points to profile
            } else {
                songCorrectLabel.setText(STRING_INCORRECT_SONG);
            }

            toBePlayedMusicLine.delete(0, toBePlayedMusicLine.length());

        });


        observableList.add(appendButton);
        observableList.add(songCorrectLabel);
        return codeBlock;
    }






}
