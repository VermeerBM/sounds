package nl.devoxx.projects.sounds.components.center.music

enum class Note (val midiNumber: Int) {
    _C(48),
    _CS(49),
    _D(50),
    _DS(51),
    _E(52),
    _F(53),
    _FS(54),
    _G(55),
    _GS(56),
    _A(57),
    _AS(58),
    _B(59),
    C(60),
    CS(61),
    D(62),
    DS(63),
    E(64),
    F(65),
    FS(66),
    G(67),
    GS(68),
    A(69),
    AS(70),
    B(71),
    C_(72),
    CS_(73),
    D_(74),
    DS_(75),
    E_(76),
    F_(77),
    FS_(78),
    G_(79),
    GS_(80),
    A_(81),
    AS_(82),
    B_(83);

    fun playNote(rowline: String) : Boolean{


        return true
    }
}
