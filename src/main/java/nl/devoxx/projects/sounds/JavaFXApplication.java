package nl.devoxx.projects.sounds;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import nl.devoxx.projects.sounds.components.center.assignment.MusicSheet;
import nl.devoxx.projects.sounds.components.left.GamePoints;
import nl.devoxx.projects.sounds.components.header.Logo;
import nl.devoxx.projects.sounds.components.center.userinput.CodeFields;

public class JavaFXApplication extends Application {

    private int i = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox();
        root.setPadding(new Insets(10));
        root.setSpacing(5);
        ObservableList<Node> children = root.getChildren();

        BackgroundFill myBF = new BackgroundFill(Color.WHITE, new CornerRadii(1),
                new Insets(0.0,0.0,0.0,0.0));// or null for the padding
        root.setBackground(new Background(myBF));

        // Component: UserInput
        children.addAll(new MusicSheet().createAssignment());
        children.addAll(new CodeFields().createUserInput());

        BorderPane border = new BorderPane();
        border.setTop(new HBox(new Logo().createHeader()));
        border.setLeft(new GamePoints().createLeftBox());
        border.setCenter(root);

        Scene scene = new Scene(border, 900, 750);
        primaryStage.setTitle("Devoxx4Kids Sounds");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
